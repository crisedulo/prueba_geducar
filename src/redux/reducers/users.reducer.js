import * as Actions from '../actions'

const initialState = {
  usersData: null,
  postsUser: null,
  postComments: null
}

const users = function (state = initialState, action) {
  switch (action.type) {
    case Actions.GET_USERS:
    {
      return {
        ...state,
        usersData: action.payload
      }
    }
    case Actions.GET_POSTS_USER:
    {
      return {
        ...state,
        postsUser: action.payload
      }
    }
    case Actions.GET_POSTS_COMMENTS:
    {
      return {
        ...state,
        postComments: action.payload
      }
    }
    case Actions.CLEAR_POST_COMMETS:
    {
      return {
        ...state,
        postComments: null
      }
    }
    default:
    {
      return state
    }
  }
}

export default users
