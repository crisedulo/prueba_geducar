import { configureStore } from '@reduxjs/toolkit'
import users from './users.reducer'

export default configureStore({
  reducer: {
    users
  }
})
