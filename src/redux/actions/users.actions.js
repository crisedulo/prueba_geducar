import axios from 'axios'

export const GET_USERS = 'GET USERS'
export const GET_POSTS_USER = 'GET POSTS USER'
export const GET_POSTS_COMMENTS = 'GET POST COMMENTS'
export const CLEAR_POST_COMMETS = 'CLEAR POSTS COMMENTS'

export function getUsers () {
  const request = axios.get('https://jsonplaceholder.typicode.com/users')

  return (dispatch) =>
    request.then((response) =>
      dispatch({
        type: GET_USERS,
        payload: response.data
      })
    )
}

export function getPostsUser (idUser) {
  const request = axios.get(`https://jsonplaceholder.typicode.com/users/${idUser}/posts`)

  return (dispatch) =>
    request.then((response) =>
      dispatch({
        type: GET_POSTS_USER,
        payload: response.data
      })
    )
}

export function getPostComments (idPost) {
  const request = axios.get(`https://jsonplaceholder.typicode.com/posts/${idPost}/comments`)

  return (dispatch) =>
    request.then((response) =>
      dispatch({
        type: GET_POSTS_COMMENTS,
        payload: response.data
      })
    )
}

export function clearPostComments (params) {
  return {
    type: CLEAR_POST_COMMETS
  }
}
