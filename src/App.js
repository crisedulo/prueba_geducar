import React from 'react'
import {
  HashRouter as Router,
  Switch,
  Route
} from 'react-router-dom'
import ListOfUsers from './components/ListOfUsers'
import UserPosts from './components/ListOfUserPosts'
import Header from './components/Header'
import Footer from './components/Footer'
import CssBaseline from '@material-ui/core/CssBaseline'
import Container from '@material-ui/core/Container'

const sections = [
  { title: 'Technology', url: '#' },
  { title: 'Design', url: '#' },
  { title: 'Culture', url: '#' },
  { title: 'Business', url: '#' }
]

export default function App () {
  return (
    <Router>
      <CssBaseline />
      <Container maxWidth="lg">
      <Header title="Users" sections={sections} />
          <Switch>
            <Route exact path="/">
              <ListOfUsers />
            </Route>
            <Route path="/user/:idUser/posts">
              <UserPosts />
            </Route>
          </Switch>
      </Container>
      <Footer title="Footer" description="Hablar es barato. Enséñame el código - Linus Torvalds" />
    </Router>
  )
}
