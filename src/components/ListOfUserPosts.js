import React, { useEffect } from 'react'
import { useParams } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'
import * as Actions from '../redux/actions'
import Grid from '@material-ui/core/Grid'
import CardPost from './CardPost'

export default function UserPosts () {
  const dispatch = useDispatch()
  const { idUser } = useParams()
  const postsUser = useSelector(state => state.users.postsUser)
  const postComments = useSelector(state => state.users.postComments)

  useEffect(() => {
    dispatch(Actions.getPostsUser(idUser))
  }, [dispatch, idUser])

  return (
    <React.Fragment>
      <Grid container spacing={4}>
        {postsUser?.map((post) => (
          <CardPost key={post.id} data={post} postComments = {postComments} />
        ))}
      </Grid>
    </React.Fragment>
  )
}
