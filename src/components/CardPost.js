import React from 'react'
import PropTypes from 'prop-types'
import { makeStyles } from '@material-ui/core/styles'
import Typography from '@material-ui/core/Typography'
import Grid from '@material-ui/core/Grid'
import Card from '@material-ui/core/Card'
import IconButton from '@material-ui/core/IconButton'
import CardContent from '@material-ui/core/CardContent'
import CardActions from '@material-ui/core/CardActions'
import ExpandMoreIcon from '@material-ui/icons/ExpandMore'
import Collapse from '@material-ui/core/Collapse'
import clsx from 'clsx'
import { useDispatch } from 'react-redux'
import * as Actions from '../redux/actions'

const useStyles = makeStyles((theme) => ({
  card: {
    display: 'flex'
  },
  cardDetails: {
    flex: 1
  },
  cardMedia: {
    width: 160
  },
  expand: {
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest
    })
  },
  expandOpen: {
    transform: 'rotate(180deg)'
  }
}))

export default function FeaturedPost (props) {
  const classes = useStyles()
  const dispatch = useDispatch()
  const { data, postComments } = props
  const [expanded, setExpanded] = React.useState(false)

  const handleExpandClick = () => {
    setExpanded(!expanded)
    !expanded ? dispatch(Actions.getPostComments(data.id)) : dispatch(Actions.clearPostComments(data.id))
  }

  return (
    <Grid item xs={12} md={6}>
        <Card className={classes.card}>
          <div className={classes.cardDetails}>
            <CardContent>
              <Typography component="h2" variant="h5">
                {data.title}
              </Typography>
              <Typography variant="subtitle1" paragraph>
                {data.body}
              </Typography>
            </CardContent>
            <CardActions>
            <IconButton
              className={clsx(classes.expand, {
                [classes.expandOpen]: expanded
              })}
              onClick={handleExpandClick}
              aria-expanded={expanded}
              aria-label="show more"
            >
              <ExpandMoreIcon />
            </IconButton>
            </CardActions>
            <Collapse in={expanded} timeout="auto" unmountOnExit>
              <CardContent>
                {/* contenido de los comentarios */}
                {
                  postComments
                    ? postComments.map((comment) => (
                     <Card key = { comment.id} className={classes.card}>
                         <div className={classes.cardDetails}>
                           <CardContent>
                             <Typography component="h3" variant="h5">
                               {comment.name}
                             </Typography>
                             <Typography variant="subtitle1" color="textSecondary">
                               {comment.email}
                            </Typography>
                             <Typography variant="subtitle1" paragraph>
                              {comment.body}
                             </Typography>
                          </CardContent>
                          <CardActions>
                           </CardActions>
                         </div>
                       </Card>))
                    : null
                  }
              </CardContent>
            </Collapse>
          </div>
        </Card>
    </Grid>
  )
}

FeaturedPost.propTypes = {
  data: PropTypes.object,
  postComments: PropTypes.object
}
