import React from 'react'
import PropTypes from 'prop-types'
import { makeStyles } from '@material-ui/core/styles'
import Typography from '@material-ui/core/Typography'
import Grid from '@material-ui/core/Grid'
import Card from '@material-ui/core/Card'
import Button from '@material-ui/core/Button'
import CardContent from '@material-ui/core/CardContent'
import CardActions from '@material-ui/core/CardActions'
import { Link } from 'react-router-dom'

const useStyles = makeStyles({
  card: {
    display: 'flex'
  },
  cardDetails: {
    flex: 1
  },
  cardMedia: {
    width: 160
  }
})

export default function FeaturedPost (props) {
  const classes = useStyles()
  const { data } = props

  return (
    <Grid item xs={12} md={6}>
        <Card className={classes.card}>
          <div className={classes.cardDetails}>
            <CardContent>
              <Typography component="h2" variant="h5">
                {data.name}
              </Typography>
              <Typography variant="subtitle1" color="textSecondary">
                {data.email}
              </Typography>
              <Typography variant="subtitle1" paragraph>
                {data.phone}
              </Typography>
            </CardContent>
            <CardActions>
            <Button
                size="small"
                color="primary"
                to = {`/user/${data.id}/posts`}
                component = {Link}>
                View Posts
              </Button>
            </CardActions>
          </div>
        </Card>
    </Grid>
  )
}

FeaturedPost.propTypes = {
  data: PropTypes.object
}
