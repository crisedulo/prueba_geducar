import React, { useEffect } from 'react'
import Grid from '@material-ui/core/Grid'
import CardUser from './CardUser'
import * as Actions from '../redux/actions'
import { useDispatch, useSelector } from 'react-redux'

export default function ListOfUsers () {
  const dispatch = useDispatch()
  const users = useSelector(state => state.users.usersData)

  useEffect(() => {
    dispatch(Actions.getUsers())
  }, [dispatch])

  return (
    <React.Fragment>
          <Grid container spacing={4}>
            {users?.map((user) => (
              <CardUser key={user.id} data={user} />
            ))}
          </Grid>
    </React.Fragment>
  )
}
